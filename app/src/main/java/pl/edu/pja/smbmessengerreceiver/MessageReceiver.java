package pl.edu.pja.smbmessengerreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

/**
 * Created by Piotr on 03.11.2016.
 */

public class MessageReceiver extends BroadcastReceiver {

    private static final String TAG = MessageReceiver.class.getSimpleName();

    public MessageReceiver() {

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive: received");
        Intent intentService = new Intent(context, MessagingService.class);
        String message = intent.getStringExtra(Intent.EXTRA_TEXT);
        Log.d(TAG, "onReceive: received: " + message);
        if (!TextUtils.isEmpty(message)) {
            intentService.putExtra(Intent.EXTRA_TEXT, message);
            context.startService(intentService);
        }
    }
}

