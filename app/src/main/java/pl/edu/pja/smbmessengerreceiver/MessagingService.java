package pl.edu.pja.smbmessengerreceiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.text.TextUtils;

public class MessagingService extends Service {

    public static final int NOTIFICATION_ID = 101;
    private static final String TAG = MessagingService.class.getSimpleName();
    public static final String INTENT_ACTION = "pl.edu.pja.smb2.messenger.CONSUME";


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String message = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (!TextUtils.isEmpty(message)) {
            showMessage(message);
            stopSelf();
        }
        return START_NOT_STICKY;
    }

    private void showMessage(String message) {
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Notification.Builder builder = new Notification.Builder(this);
        builder.setContentTitle(getText(R.string.app_name));
        builder.setContentText(message);
        builder.setAutoCancel(true);
        builder.setSmallIcon(R.drawable.ic_message_text_white_24dp);
        Intent contentIntent = new Intent(INTENT_ACTION);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 102, contentIntent,
                                                                PendingIntent.FLAG_CANCEL_CURRENT);
        builder.setContentIntent(pendingIntent);
        manager.notify(NOTIFICATION_ID, builder.build());
    }

    public MessagingService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
